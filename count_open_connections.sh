#!/bin/sh

if [ $# -ne 1 ]
  then
    echo "Erorr: Please provide the port number as first and only argument."
    exit 1
fi

lines=$(ss -tn src :$1 | wc -l)
conns=$(( lines - 1 ))

echo -n "$conns"
exit 0
