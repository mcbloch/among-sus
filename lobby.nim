let doc = """
Among Sus Lobby.

Usage:
  lobby [options]
  lobby (-h | --help)

Options:
  -h --help                 Show this screen.
  --port=<port>             The port the lobby listens on          [default: 1111].
  --port-range-start=<prs>  Start of the port range games spawn on [default: 50000].
  --port-range-end=<pre>    End of the port range games spawn on   [default: 60000].
"""


import db_sqlite
import asyncnet, asyncdispatch
import net
import random
import osproc
import strutils
import times
import os
import docopt

randomize()

# Args parsing
let args = docopt(doc, version = "Among Sus lobby")

let lobby_port = parseInt($args["--port"])
let port_start = parseInt($args["--port-range-start"])
let port_end = parseInt($args["--port-range-end"])


# Database
let db = open("lobby.db", "", "", "")

# db.exec(sql"DROP TABLE IF EXISTS games")
db.exec(sql"""CREATE TABLE IF NOT EXISTS games (
                 port       INTEGER unique,
                 game_pid   INTEGER unique null,
                 players    INTEGER,
                created_at DATETIME DEFAULT CURRENT_TIMESTAMP
              )""")




var clients {.threadvar.}: seq[AsyncSocket]


proc processClient(client: AsyncSocket) {.async.} =
  await client.send("[Lobby] Commands: new (start new game), list (list running games)" & "\c\L")
  while true:
    let line = await client.recvLine()
    if line.len == 0: break

    if line == "new":
      var port = 0

      let open_games = parseInt(db.getRow(sql"SELECT count(*) from games")[0])
      if open_games == port_end-port_start+1:
          echo "New game requested but port range is full."
          await client.send("Games are full. Try again later." & "\c\L")
          continue

      while true: # Will loop forever if we run out of ports.
          try:
            let num = rand(port_start..port_end)
            # echo "Trying " & $num
            db.exec(sql"INSERT INTO games (port, game_pid, players) VALUES (?, ?, 0)", $num, $0)
            # echo "Free spot"
            port = num
            break;
          except:
              discard

      echo "Starting new game on port " & $port
      await client.send("[Lobby] Starting new game on port " & $port & ". You have 30 seconds to connect. Games are stopped when everybody disconnects.\c\L")
      let p = startProcess("among-sus", args=[$port, "&"])
      let pid = p.processID()
      db.exec(sql"UPDATE games SET game_pid = ? WHERE port = ?", $pid, $port)


    elif line == "list":
      await client.send("[Lobby] Running games" & "\c\L")
      for x in db.fastRows(sql"SELECT * FROM games"):
        await client.send("    - [Port]: " & x[0] & " [Players]: " & x[2] & "\c\L")
      db.exec(sql"SELECT * FROM games")
    else:
      await client.send("[Lobby] Unknown command" & "\c\L")


#for c in clients:
    #  await c.send(line & "\c\L")

proc serve() {.async.} =
  clients = @[]
  var server = newAsyncSocket()
  server.setSockOpt(OptReuseAddr, true)
  server.bindAddr(Port(lobby_port))
  server.listen()
  echo "Lobby is listening on port " & $lobby_port & ". Will spawn games on ports in range " & $port_start & ".." & $port_end & "."

  while true:
    let client = await server.accept()
    clients.add client

    asyncCheck processClient(client)

proc count_connections() {.async.} =
  while true:
    # echo "[Purge] Checking empty games"
    for x in db.fastRows(sql"SELECT * FROM games"):
        let port = parseInt(x[0])
        let pid = parseInt(x[1])
        let players = parseInt(x[2])
        let created_at = times.parse(x[3], "yyyy-MM-dd HH:mm:ss", utc())
      
        # echo "Port: " & port & ", players: " & players

        let outp = execProcess("./count_open_connections.sh", args=[$port], options={poUsePath})
        let new_players = parseInt(strip(outp))

        if players != new_players:
          db.exec(sql"UPDATE games SET players = ? where port = ?", $new_players, $port)
          if players > new_players:
            echo "Player left game " & $port & " (" & $new_players & " players)"
          elif players < new_players:
            echo "New player in game " & $port & " (" & $new_players & " players)"

        let runtime = times.now() - created_at
        if new_players == 0 and runtime > initDuration(seconds = 30):

          # Free the port by killing the application and removing the entry from the database.
          db.exec(sql"DELETE FROM games WHERE port = ?", $port)
          echo "[Purge] Shutdown old game with pid " & $pid & " on port " & $port & ". It ran for " & $runtime & "."
          let _ = execProcess("kill", args=[$pid], options={poUsePath})

    await sleepAsync(2000)


asyncCheck serve()
asyncCheck count_connections()
runForever()

db.close()
