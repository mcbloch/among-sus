.POSIX:
all: clean among-sus lobby

among-sus:
	$(CC) -o among-sus $(CFLAGS) main.c $(LDFLAGS)

lobby:
	nim c lobby.nim

clean:
	rm -f among-sus
	rm -f lobby

.PHONY: docker docker-logs
docker:
	docker-compose down -v && docker-compose build && docker-compose up -d
docker-logs:
	docker-compose logs -f
