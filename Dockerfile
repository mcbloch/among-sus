FROM nimlang/nim:1.2.6-alpine

RUN apk add build-base
RUN apk add sqlite-libs iproute2

WORKDIR /code
COPY . .

RUN make clean

# Build lobby
RUN nimble install -y docopt
RUN nim c -d:release lobby.nim

# Build main game
RUN make among-sus

ENTRYPOINT ["./lobby"]
